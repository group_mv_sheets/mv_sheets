---
title: 'Übungszettel '
author: "Johannes Brachem (jobrachem@posteo.de)"
subtitle: M.Psy.205, Sommersemester 2018
output:
  html_document:
    theme: paper
    toc: yes
    toc_depth: 2
    toc_float: FALSE
header-includes:
 - \usepackage{comment}
params:
 soln: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

**Hinweise zur Bearbeitung**

1. Bitte beantworten Sie die Fragen direkt in der .Rmd Datei. [Hier können Sie sie herunterladen](https://owncloud.gwdg.de/index.php/s/Q5f84vye45oTCqG/download) 
2. Informationen, die Sie für die Bearbeitung benötigen, finden Sie auf der [Website der Veranstaltung](https://www.psych.uni-goettingen.de/de/it/team/zezula/courses/multivariate)
3. Zögern Sie nicht, im Internet nach Lösungen zu suchen. Das effektive Suchen nach Lösungen für R-Probleme im Internet ist tatsächlich eine sehr nützliche Fähigkeit, auch Profis arbeiten auf diese Weise. Die beste Anlaufstelle dafür ist der [R-Bereich der Programmiererplattform Stackoverflow](https://stackoverflow.com/questions/tagged/r)
4. Auf der Website von R Studio finden Sie sehr [hilfreiche Übersichtszettel](https://www.rstudio.com/resources/cheatsheets/) zu vielen verschiedenen R-bezogenen Themen. Ein guter Anfang ist der [Base R Cheat Sheet](http://github.com/rstudio/cheatsheets/raw/master/base-r.pdf)



`r if(!params$soln) {"\\begin{comment}"}`
### Lösung

`r if(!params$soln) {"\\end{comment}"}`


*Anmerkung*: Diese Übungszettel basieren zum Teil auf den "Smart Alex" Aufgaben aus dem Lehrbuch *Dicovering Statistics Using R* (Field, Miles & Field, 2012). Sie wurden für den Zweck dieser Übung modifiziert, und der verwendete R-Code wurde aktualisiert.

# Literatur
Field, A., Miles, J., & Field, Z. (2012). *Discovering Statistics Using R*. London: SAGE Publications Ltd.